package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		
		assertTrue("Invalid login name" , LoginValidator.isValidLoginName( "DilrajSingh" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		
		assertTrue("Invalid login name" , LoginValidator.isValidLoginName( "dilraj" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		
		assertFalse("Invalid login name" , LoginValidator.isValidLoginName( "dilra" ) );
	}
	

	@Test
	public void testIsValidLoginLengthBoundaryException( ) {
		
		//user added user special characters
		assertFalse("Invalid login name" , LoginValidator.isValidLoginName( "dilra$" ) );
	}
	
	// character tests
	
	@Test
	public void testIsValidLoginCharactersRegular( ) {
		
		assertTrue("Invalid login name" , LoginValidator.isValidLoginName( "Dilraj99" ) );
	}
	
	@Test
	public void testIsValidLoginCharacterBoundaryIn( ) {
		
		assertTrue("Invalid login name" , LoginValidator.isValidLoginName( "dil200" ) );
	}
	
	@Test
	public void testIsValidLoginCharacterBoundaryOut( ) {
		
		assertFalse("Invalid login name" , LoginValidator.isValidLoginName( "dil_00" ) );
	}
	

	@Test
	public void testIsValidLoginCharacterBoundaryException( ) {
		
		//user added user special characters
		assertFalse("Invalid login name" , LoginValidator.isValidLoginName( "$%$%$%$" ) );
	}

}
